<?php
namespace App\Controller\Api;

use App\Entity\History;
use App\Repository\HistoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;


#[Route('/api/exchange', name: 'app_exchange', methods: ['POST'])]
class ExchangeController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private HistoryRepository $historyRepository;
    public function __construct(EntityManagerInterface $entityManager, HistoryRepository $historyRepository )
    {
        $this->entityManager = $entityManager;
        $this->historyRepository = $historyRepository;
    }

    #[Route('/values', name: 'app_values', methods: ['POST'])]
    public function exchangeValues(Request $request): JsonResponse
    {
        $requestData = json_decode($request->getContent(), true);

        if (isset($requestData['first']) && isset($requestData['second'])) {
            $first = (int) $requestData['first'];
            $second = (int) $requestData['second'];

            $first = $first + $second;
            $second = $first - $second;
            $first = $first - $second;

            $history = new History();
            $history
                ->setFirstIn($requestData['first'])
                ->setSecondIn($requestData['second'])
                ->setFirstOut($first)
                ->setSecondOut($second)
                ->setCreatedAt(new \DateTimeImmutable())
                ->setUpdatedAt(new \DateTimeImmutable());

            $this->entityManager->persist($history);
            $this->entityManager->flush();

            return $this->json([
                'first' => $first,
                'second' => $second,
            ]);
        }

        return $this->json(['error' => 'Missing parameters'], 400);
    }

    #[Route('/history', name: 'app_history', methods: ['GET', 'POST'])]
    public function getHistory(Request $request): JsonResponse
    {
        $page = $request->query->getInt('page', 1);
        $perPage = $request->query->getInt('perPage', 10);
        $sortBy = $request->query->get('sortBy', 'createdAt');
        $sortOrder = $request->query->get('sortOrder', 'ASC');

        $paginator = $this->historyRepository->getPaginatedAndSortedData($page, $perPage, $sortBy, $sortOrder);

        $data = [
            'totalItems' => count($paginator),
            'currentPage' => $page,
            'itemsPerPage' => $perPage,
            'data' => iterator_to_array($paginator),
        ];

        return $this->json($data);
    }



}
